import React, { useEffect, useState } from 'react';

function ConferenceForm () {
    const [location, setLocation] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    });

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (conferenceUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            });
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocation(data.locations)
            }
        }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                        <label htmlFor="name">Starts</label>
                    </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                        <label htmlFor="name">Ends</label>
                    </div>
                        <div className="mb-3">
                        <label htmlFor="name">Description</label>
                        <textarea onChange={handleFormChange} value={formData.description} required type="text" rows="3" name="description" id="description" className="form-control"></textarea>
                    </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                        <label htmlFor="room_count">Maximum presentations</label>
                    </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                        <label htmlFor="room_count">Maximum attendees</label>
                    </div>
                        <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.location} required id="location" name="location" className="form-select">
                            <option value="">Choose a location</option>
                            {location.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;
