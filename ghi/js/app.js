function createCard(name, description, pictureUrl, starts, ends, location) {
    const formattedStarts = new Date(starts).toLocaleDateString('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' });
    const formattedEnds = new Date(ends).toLocaleDateString('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' });
    return `
      <div class="card" style="box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);"> <!-- Add boxShadow styling -->>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer"><div class="card-footer">${formattedStarts} - ${formattedEnds}</div>
        </div>
      </div>
    `;
  }

function redAlert() {
    return `<div class="alert alert-warning" role="alert">
    This is a Warning!
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        let badAlert = document.querySelector('h2')
        badAlert.innerHTML += redAlert()

      } else {
        const data = await response.json();
let index = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            const column = document.querySelector('.col');
            const column2 = document.querySelector('.col:nth-child(2)');
            const column3 = document.querySelector('.col:nth-child(3)');
            if (index % 3 == 0) {
                column.innerHTML += html;
            } else if (index % 3 == 1) {
                column2.innerHTML += html;
            } else if (index % 3 == 2) {
                column3.innerHTML += html;
            }
          }
          index++
        }
      }
    } catch (e) {
        console.error('error', e);
      // Figure out what to do if an error is raised
    }

  });
