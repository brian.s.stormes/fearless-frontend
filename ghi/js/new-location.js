
window.addEventListener('DOMContentLoaded', async () => {

    const stateUrl = 'http://localhost:8000/api/states/';
    const stateResponse = await fetch(stateUrl);
    if (stateResponse.ok) {
        const stateData = await stateResponse.json();
    // Get the select tag element by its id 'state'
        const selectTag = document.getElementById('state');
    // For each state in the states property of the data
        for (const state of stateData.states) {
             // Create an 'option' element
            // Set the '.value' property of the option element to the
            // state's abbreviation
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
    }
}
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event =>{
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
        }
    });
  // Set the '.innerHTML' property of the option element to
  // the state's name
  // Append the option element as a child of the select tag

});
